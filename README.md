# Ntx20.Py



## Install

```
pip install aiofiles python-dotenv
pip install --upgrade --pre "grpclib[protobuf]"
```

## Optionally regenerate grpc client
See [grpclib](https://github.com/vmagamedov/grpclib)

```
pip install grpcio-tools 
python -m grpc_tools.protoc -I. --python_out=. --grpclib_python_out=. proto/engine.proto
``` 
## Run trasncription
Set any environment variable eg. `myng2=https://usr:psw@example.com` 
```
python ntx20.py --input test.wav --rewriter cz-atran-openlex@myng2
```


