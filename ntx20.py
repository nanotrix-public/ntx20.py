import argparse
import asyncio
import logging
import os
import platform
from typing import AsyncGenerator, AsyncIterator, Generator, List

import aiofiles
from dotenv import load_dotenv
from grpclib.client import Channel, Stream
from urllib.parse import urlparse
from base64 import b64encode
from proto.engine_pb2 import Payload,Item
from proto.engine_grpc import EngineServiceStub
import atexit

from rewrite import Rewriter,MultipartStream

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input",
        type=str,
        required=True,
        help="""Input audio file""",
    )
    parser.add_argument(
        "--rewriter",
        type=str,
        required=True,
        help="""rewriter service:version@cluster""",
    )
    parser.add_argument(
        "--track",
        type=str,
        default="pnc",
        help="""one of vad,v2t,ppc,pnc""",
    )
   
    return parser.parse_args()


async def multipart(args,contents,n):
    #for more details see https://gitlab.com/nanotrix-public/ntx20.git
    config = Payload(
        track = "ctrl:start",
        chunk=[
            Item(key="audio-format",s="auto:0",type="s"),
            Item(key="audio-channel",s="downmix",type="s"),
            Item(key="features",s="novad",type="s")
        ])
    
    with Rewriter(args.rewriter) as rewriter:
        async with rewriter.GetStream() as stream:
            for i in range(0,n):
                async for payload in stream.rewrite_bytes(contents,config=config):
                    if payload.track==args.track:
                        for item in payload.chunk:
                            if "la" in item.tags:
                                continue
                            if "noise" in item.tags:
                                continue
                            if item.key=="txt":
                                print(item.s, flush=True,end='')


async def rewrite(args,contents):
    config = Payload(
        chunk=[
            Item(key="audio-format",s="auto:0",type="s"),
            Item(key="audio-channel",s="downmix",type="s"),
            Item(key="features",s="novad",type="s")
        ])
     
     #create only one rewriter and run many async rewrite_file tasks in parallel
    with Rewriter(args.rewriter) as rewriter:
        async for payload in rewriter.rewrite_bytes(contents,config=config):
            if payload.track==args.track:
                for item in payload.chunk:
                    if "la" in item.tags:
                        continue
                    if "noise" in item.tags:
                        continue
                    if item.key=="txt":
                        print(item.s, flush=True,end='')


async def main():
    args = get_args()
    load_dotenv()
    with open(args.input, mode="rb") as file:
        contents = file.read()
    await rewrite(args, contents)




if __name__ == '__main__':
    formatter = (
        "%(asctime)s %(levelname)s [%(filename)s:%(lineno)d] %(message)s"
    )
    logging.basicConfig(format=formatter, level=logging.INFO)
    #test()
    #print(timeit.timeit("test()", globals=globals(),number=25000))
    if platform.system()=='Windows':
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
        loop = asyncio.ProactorEventLoop()
        loop.run_until_complete(main())
    else:
        asyncio.run(main()) 