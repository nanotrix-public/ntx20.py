# Generated by the Protocol Buffers compiler. DO NOT EDIT!
# source: proto/engine.proto
# plugin: grpclib.plugin.main
import abc
import typing

import grpclib.const
import grpclib.client
if typing.TYPE_CHECKING:
    import grpclib.server

import proto.engine_pb2


class EngineServiceBase(abc.ABC):

    @abc.abstractmethod
    async def Streaming(self, stream: 'grpclib.server.Stream[proto.engine_pb2.Payload, proto.engine_pb2.Payload]') -> None:
        pass

    def __mapping__(self) -> typing.Dict[str, grpclib.const.Handler]:
        return {
            '/ntx.core20.EngineService/Streaming': grpclib.const.Handler(
                self.Streaming,
                grpclib.const.Cardinality.STREAM_STREAM,
                proto.engine_pb2.Payload,
                proto.engine_pb2.Payload,
            ),
        }


class EngineServiceStub:

    def __init__(self, channel: grpclib.client.Channel) -> None:
        self.Streaming = grpclib.client.StreamStreamMethod(
            channel,
            '/ntx.core20.EngineService/Streaming',
            proto.engine_pb2.Payload,
            proto.engine_pb2.Payload,
        )
