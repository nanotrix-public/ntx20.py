import asyncio
import os
from typing import AsyncGenerator, AsyncIterator, Generator, List, Type

import aiofiles
from grpclib.client import Channel, Stream
from urllib.parse import urlparse
from base64 import b64encode
from proto.engine_pb2 import Payload,Item
from proto.engine_grpc import EngineServiceStub
import atexit

def basic_auth(username, password):
    token = b64encode(f"{username}:{password}".encode('utf-8')).decode("ascii")
    return f'Basic {token}'

async def upstream(stream,f, autocomplete=True) -> None:
        while True:
            inp = await f.read(4096)
            if not inp: break
            await stream.send_message(Payload(track='aud', chunk=[Item(b=inp)]))
        if autocomplete:
            await stream.end()
        else:
            await stream.send_message(Payload(track='ctrl:end'))

def chunked(size, source):
    for i in range(0, len(source), size):
        yield source[i:i+size]

async def data_upstream(stream, data, autocomplete=True):
    for c in chunked(4096, data):
        await stream.send_message(Payload(track='aud', chunk=[Item(b=c)]))
    if autocomplete:
        await stream.end()
    else:
        await stream.send_message(Payload(track='ctrl:end'))

def get_track_text(transcript: List[Payload], track: str) -> str:
    ret =""
    for payload in transcript:
        if payload.track==track:
                for item in payload.chunk:
                    if "la" in item.tags:
                        continue
                    if "noise" in item.tags:
                        continue
                    if item.key=="txt":
                        ret += item.s
    return ret                    


class Rewriter:
    def __init__(self, connection_string: str):
        service, connection = connection_string.split("@",maxsplit=1)
        cs = os.getenv(connection)
        if cs is not None:
            connection = cs

        parsed = urlparse(connection)
        auth_token = basic_auth(parsed.username,parsed.password)
        is_ssl = connection.startswith("https:")
        port_ = parsed.port
        if port_ is None:
            port_ = 443 if is_ssl else 80
        self.channel = Channel(host=parsed.hostname,port=port_,ssl=is_ssl)
        self.metadata={'authorization': auth_token, 'service' : service}

    def __enter__(self):
        return self

    async def rewrite_file(self, file: os.PathLike, config: Type[Payload]) -> AsyncIterator[Payload]:
       
        stub = EngineServiceStub(self.channel)
        async with stub.Streaming.open(metadata=self.metadata) as stream:
            await stream.send_message(config)
            reply = await stream.recv_message()
            async with aiofiles.open(file, mode='rb') as f:
                up_task = asyncio.create_task(upstream(stream,f))
                async for reply in stream:
                    yield reply
                await up_task
    def GetStream(self):
        return MultipartStream(self.channel,self.metadata)
    
    async def rewrite_bytes(self, data: bytes, config: Type[Payload]) -> AsyncIterator[Payload]:
        
        stub = EngineServiceStub(self.channel)
        async with stub.Streaming.open(metadata=self.metadata) as stream:
            try:
                await stream.send_message(config)
                await stream.recv_initial_metadata()
                reply = await stream.recv_message()
                
                up_task = asyncio.create_task(data_upstream(stream,data))
                async for reply in stream:
                    yield reply
                await up_task
            except Exception as e:
                print(stream.initial_metadata)
                raise Exception(str(e) + "with meta" + stream.initial_metadata)
    
    def __exit__(self, exception_type, exception_value, traceback):
        self.channel.close()

class MultipartStream:
    def __init__(self, channel, imeta):
        self.channel = channel
        self.imeta = imeta
        self.metadata = None
    async def __aenter__(self):
        self.stream = await EngineServiceStub(self.channel).Streaming.open(metadata=self.imeta).__aenter__()
        return self
    

    async def rewrite_file(self, file: os.PathLike, config: Type[Payload]) -> AsyncIterator[Payload]:
       
        
        await self.stream.send_message(config)
        if self.metadata is None:
            await self.stream.recv_initial_metadata()
            self.metadata= self.stream.initial_metadata
        reply = await self.stream.recv_message()
        async with aiofiles.open(file, mode='rb') as f:
            up_task = asyncio.create_task(upstream(self.stream,f,False))
            async for reply in self.stream:
                if reply.track=='ctrl:end':
                    break
                yield reply 
            await up_task
        
    
    async def rewrite_bytes(self, data: bytes, config: Type[Payload]) -> AsyncIterator[Payload]:
        
        try:
            await self.stream.send_message(config)
            if self.metadata is None:
                await self.stream.recv_initial_metadata()
                self.metadata= self.stream.initial_metadata
            reply = await self.stream.recv_message()
            up_task = asyncio.create_task(data_upstream(self.stream,data,False))
            async for reply in self.stream:
                if reply.track=='ctrl:end':
                    break
                yield reply
            await up_task
        except Exception as e:
            print(self.metadata)
            raise Exception(str(e) + f"with meta + {self.metadata}")
    
    async def __aexit__(self, exc_type, exc, tb):
        
        await self.stream.end()
        await self.stream.__aexit__(exc_type,exc,tb)
        